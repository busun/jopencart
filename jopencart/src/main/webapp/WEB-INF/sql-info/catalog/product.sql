#namespace("catalog.catalog.product")
	###获取商品总数
	#sql("getTotalProducts")
		SELECT COUNT( DISTINCT p.product_id ) AS total
		#if(notBlank(filter_category_id))
			#if(notBlank(filter_sub_category))
			FROM #(DB_PREFIX)category_path cp LEFT JOIN #(DB_PREFIX)product_to_category p2c ON ( cp.category_id = p2c.category_id )
			#else
			FROM #(DB_PREFIX)product_to_category p2c
			#end
			#if(notBlank(filter_filter))
			LEFT JOIN #(DB_PREFIX)product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN #(DB_PREFIX)product p ON (pf.product_id = p.product_id)
			#else
			LEFT JOIN #(DB_PREFIX)product p ON (p2c.product_id = p.product_id)
			#end
		#else
			FROM #(DB_PREFIX)product p
		#end
		LEFT JOIN #(DB_PREFIX)product_description pd ON (p.product_id = pd.product_id) LEFT JOIN #(DB_PREFIX)product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = #p(config_language_id)   AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0
		#if(notBlank(filter_category_id))
			#if(notBlank(filter_sub_category))
			AND cp.path_id = #p(filter_category_id)
			#else
			AND p2c.category_id = #p(filter_category_id)
			#end
			#if(notBlank(filter_filter))
				AND pf.filter_id IN (
				#for(filter_id : filter_filter)
					#(for.index > 0 ? "," : "") #p(filter_id)
				#end
				)
			#end
		#end
		#if(notBlank(filter_manufacturer_id))
			AND p.manufacturer_id = #p(filter_manufacturer_id)
		#end
	#end;
	
	###获取商品
	#sql("getProducts")
        SELECT p.product_id, (SELECT AVG( rating ) AS total FROM #(DB_PREFIX)review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id ) AS rating,
			(SELECT price FROM #(DB_PREFIX)product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = 1 AND pd2.quantity = '1' AND( (pd2.date_start = '0000-00-00' OR pd2.date_start < NOW() ) AND( pd2.date_end = '0000-00-00' OR pd2.date_end > NOW() ))
				ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1 ) AS discount,
			(SELECT price FROM #(DB_PREFIX)product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = 1 AND((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND(ps.date_end = '0000-00-00' OR ps.date_end > NOW()))
				ORDER BY ps.priority ASC, ps.price ASC LIMIT 1 ) AS special
		#if(notBlank(filter_category_id))
			#if(notBlank(filter_sub_category))
			FROM #(DB_PREFIX)category_path cp LEFT JOIN #(DB_PREFIX)product_to_category p2c ON ( cp.category_id = p2c.category_id )
			#else
			FROM #(DB_PREFIX)product_to_category p2c
			#end
			#if(notBlank(filter_filter))
			LEFT JOIN #(DB_PREFIX)product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN #(DB_PREFIX)product p ON (pf.product_id = p.product_id)
			#else
			LEFT JOIN #(DB_PREFIX)product p ON (p2c.product_id = p.product_id)
			#end
		#else
			FROM #(DB_PREFIX)product p		
		#end
		LEFT JOIN #(DB_PREFIX)product_description pd ON (p.product_id = pd.product_id) LEFT JOIN #(DB_PREFIX)product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = #p(config_language_id)  AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = 0
		#if(notBlank(filter_category_id))
			#if(notBlank(filter_sub_category))
			AND cp.path_id = #p(filter_category_id)
			#else
			AND p2c.category_id = #p(filter_category_id)
			#end
			#if(notBlank(filter_filter))
				AND pf.filter_id IN (
				#for(filter_id : filter_filter)
					#(for.index > 0 ? "," : "") #p(filter_id)
				#end
				)
			#end
		#end
		#if(notBlank(filter_manufacturer_id))
			AND p.manufacturer_id = #p(filter_manufacturer_id)
		#end
		GROUP BY p.product_id  ORDER BY p.sort_order LIMIT #p(start) , #p(limit)
	#end
#end;