package cn.javacart.jopencart.library;

import javax.servlet.http.HttpServletRequest;

public class RequestService {
	
	static ThreadLocal<HttpServletRequest> threadLocal = new ThreadLocal<HttpServletRequest>();

	
	/**
	 * 初始化
	 * @param session
	 */
	public static void init(HttpServletRequest request){
		threadLocal.set(request);
	}
	
	public static HttpServletRequest get(){
		return threadLocal.get();
	}
	
	/**
	 * 重置
	 */
	public static void reset(){
		threadLocal.set(null);
	}
	
}
