package cn.javacart.jopencart.library;

import javax.servlet.http.HttpSession;

public class SessionService {
	
	static ThreadLocal<HttpSession> threadLocal = new ThreadLocal<HttpSession>();
	
	/**
	 * 初始化
	 * @param session
	 */
	public static void init(HttpSession session){
		threadLocal.set(session);
	}
	
	public static HttpSession get(){
		return threadLocal.get();
	}
	
	/**
	 * 重置
	 */
	public static void reset(){
		threadLocal.set(null);
	}
	
}
