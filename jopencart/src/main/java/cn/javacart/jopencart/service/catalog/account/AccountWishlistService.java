package cn.javacart.jopencart.service.catalog.account;

import java.util.Date;

import cn.javacart.jopencart.library.cart.CustomerService;
import cn.javacart.jopencart.model.CustomerWishlist;

import com.jfinal.aop.Duang;

/**
 * 愿望清单
 * @author farmer
 *
 */
public class AccountWishlistService {

	public final static AccountWishlistService ME = Duang.duang(AccountWishlistService.class);
	
	/**
	 * 添加愿望清单
	 * @param productId
	 */
	public void addWishlist(Integer productId){
		Integer customerId = CustomerService.me().getCustomerId();
		CustomerWishlist customerWishlist = CustomerWishlist.ME.findFirst("select * from joc_customer_wishlist w where w.customer_id = ? and product_id = ?",customerId ,productId);
		if(customerWishlist == null){
			new CustomerWishlist().set("customer_id", customerId)
					.set("product_id", productId)
					.set("date_added", new Date()).save();
		}else{
			customerWishlist.set("customer_id", customerId)
					.set("product_id", productId)
					.set("date_added", new Date()).update();			
		}
	} 
	
}
