package cn.javacart.jopencart.service.catalog.catalog;

import java.util.List;

import cn.javacart.jopencart.library.SessionConfigService;
import cn.javacart.jopencart.model.Category;

import com.jfinal.aop.Duang;

/**
 * 分类服务
 * @author farmer
 *
 */
public class CatalogCategoryService {

	public final static CatalogCategoryService ME = Duang.duang(CatalogCategoryService.class);

	/**
	 * 获取分类根据分类ID
	 * @param categoryId
	 * 	分类ID
	 */
	public Category getCategory(Integer categoryId) {
		return Category.ME.findFirst("SELECT DISTINCT * FROM joc_category c LEFT JOIN joc_category_description cd ON (c.category_id = cd.category_id) LEFT JOIN joc_category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = ? AND cd.language_id = ? AND c2s.store_id = ? AND c.status = '1'", 
					categoryId,SessionConfigService.get("config_language_id"),0);
	}

	/**
	 * 获取子分类
	 * @param categoryId
	 * @return
	 */
	public List<Category> getCategories(Integer categoryId) {
		String sql = "SELECT\n" + 
				"	*\n" + 
				"FROM\n" + 
				"	joc_category c\n" + 
				"LEFT JOIN joc_category_description cd ON\n" + 
				"	(\n" + 
				"		c.category_id = cd.category_id\n" + 
				"	)\n" + 
				"LEFT JOIN joc_category_to_store c2s ON\n" + 
				"	(\n" + 
				"		c.category_id = c2s.category_id\n" + 
				"	)\n" + 
				"WHERE\n" + 
				"	c.parent_id = ?\n" + 
				"	AND cd.language_id = ?\n" + 
				"	AND c2s.store_id = '0'\n" + 
				"	AND c.status = '1'\n" + 
				"ORDER BY\n" + 
				"	c.sort_order,\n" + 
				"	LCASE( cd.name )";
		return Category.ME.find(sql,categoryId,SessionConfigService.get("config_language_id"));
	}
	
}
