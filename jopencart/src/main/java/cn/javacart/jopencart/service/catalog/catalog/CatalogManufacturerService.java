package cn.javacart.jopencart.service.catalog.catalog;

import com.jfinal.aop.Duang;

import cn.javacart.jopencart.model.Manufacturer;

public class CatalogManufacturerService {
	
	public final static CatalogManufacturerService ME = Duang.duang(CatalogManufacturerService.class);
	
	/**
	 * 获取供应商信息
	 * @param manufacturerId
	 * @return
	 */
	public Manufacturer getManufacturer(Integer manufacturerId){
		return Manufacturer.ME.findFirst("SELECT * FROM joc_manufacturer m LEFT JOIN joc_manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m.manufacturer_id = ? AND m2s.store_id = '0'", manufacturerId);
	}
}
