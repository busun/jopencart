package cn.javacart.jopencart.controller.catalog.account;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.jfinal.aop.Before;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnLeftModule;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnRightModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentBottomModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentTopModule;
import cn.javacart.jopencart.controller.catalog.common.CommonFooterModule;
import cn.javacart.jopencart.controller.catalog.common.CommonHeaderModule;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.service.catalog.account.AccountActivityService;
import cn.javacart.jopencart.service.catalog.account.AccountAddressService;
import cn.javacart.jopencart.service.catalog.account.AccountWishlistService;
import cn.javacart.jopencart.util.ChainMap;
import cn.javacart.jopencart.validator.AccountLoginValidator;

/**
 * 登录
 * @author farmer
 *
 */
public class AccountLoginController extends JOpencartController{

	/**
	 * 登录界面
	 */
	@Before(AccountLoginValidator.class)
	public void index(){
		if (customer.isLogged()) {
			if(isPost()){	//提交登录并且校验通过//
				removeSessionAttr("guest");
				if("payment".equals(config.get("config_tax_customer")))
				{					
					setSessionAttr("payment_address", AccountAddressService.ME.getAddress(customer.getAddressId()));
				}
				if("shipping".equals(config.get("config_tax_customer")))
				{					
					setSessionAttr("shipping_address", AccountAddressService.ME.getAddress(customer.getAddressId()));
				}
				//愿望清单更新
				Map<Integer, Integer> wishlist = getSessionAttr("wishlist");
				if(getSessionAttr("wishlist") != null){
					for(Entry<Integer, Integer> entry : wishlist.entrySet()){
						AccountWishlistService.ME.addWishlist(entry.getValue());
					}
					wishlist.clear();
				}
				//添加用户登录记录
				AccountActivityService.ME.addActivity("login", 
						ChainMap.createMap().put("customer_id", customer.getCustomerId())
						.put("name", customer.getFirstname()+" " + customer.getLastname()).toMap());
				//TODO Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
				/*
				 * 
				 * 
				 * 
				 */
			}
			redirect(url.linkStr("account/account", null));
			return;
		}
		//加载语言
		language.load("/catalog/language/{0}/account/login.php");
		document.setTitle(language.getStr("heading_title"));
		List<Map<String, Object>> breadcrumbs = new ArrayList<Map<String,Object>>();
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_home")).put("href", url.linkStr("common/home", null)).toMap());
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_account")).put("href", url.linkStr("account/account", null)).toMap());
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_login")).put("href", url.linkStr("account/login", null)).toMap());
		dataMap.put("breadcrumbs", MemoryUtils.valueOf(breadcrumbs));
		dataMap.put("heading_title",language.get("heading_title"));
		dataMap.put("text_new_customer",language.get("text_new_customer"));
		dataMap.put("text_register",language.get("text_register"));
		dataMap.put("text_register_account",language.get("text_register_account"));
		dataMap.put("text_returning_customer", language.get("text_returning_customer"));
		dataMap.put("text_i_am_returning_customer", language.get("text_i_am_returning_customer"));
		dataMap.put("text_forgotten", language.get("text_forgotten"));
		dataMap.put("entry_email", language.get("entry_email"));
		dataMap.put("entry_password", language.get("entry_password"));
		dataMap.put("button_continue", language.get("button_continue"));
		dataMap.put("button_login", language.get("button_login"));
		
		//错误信息
		if(errorMap.size() > 0){			
			dataMap.put("error_warning", MemoryUtils.valueOf(errorMap.get("warning")));
		}else{
			dataMap.put("error_warning", MemoryUtils.valueOf(""));
		}
		dataMap.put("action" , url.link("account/login",null));
		dataMap.put("register" , url.link("account/register",null));
		dataMap.put("forgotten" , url.link("account/forgotten",null));
		
		dataMap.put("column_left", MemoryUtils.valueOf(load.module(new CommonColumnLeftModule(this))));
		dataMap.put("column_right", MemoryUtils.valueOf(load.module(new CommonColumnRightModule(this))));
		dataMap.put("content_top", MemoryUtils.valueOf(load.module(new CommonContentTopModule(this))));
		dataMap.put("content_bottom", MemoryUtils.valueOf(load.module(new CommonContentBottomModule(this))));
		dataMap.put("footer", MemoryUtils.valueOf(load.module(new CommonFooterModule(this))));
		dataMap.put("header", MemoryUtils.valueOf(load.module(new CommonHeaderModule(this))));
		render(new PhpRender("/catalog/view/theme/default/template/account/login.tpl", dataMap));
	}
	
	
}
