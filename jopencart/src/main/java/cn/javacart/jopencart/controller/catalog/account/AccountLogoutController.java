package cn.javacart.jopencart.controller.catalog.account;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnLeftModule;
import cn.javacart.jopencart.controller.catalog.common.CommonColumnRightModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentBottomModule;
import cn.javacart.jopencart.controller.catalog.common.CommonContentTopModule;
import cn.javacart.jopencart.controller.catalog.common.CommonFooterModule;
import cn.javacart.jopencart.controller.catalog.common.CommonHeaderModule;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.util.ChainMap;

/**
 * 注销
 * @author farmer
 *
 */
public class AccountLogoutController extends JOpencartController{

	/**
	 * 注销请求
	 */
	public void index(){
		if(customer.isLogged()){
			customer.logout();
			removeSessionAttr("shipping_address");
			removeSessionAttr("shipping_method");
			removeSessionAttr("shipping_methods");
			removeSessionAttr("payment_address");
			removeSessionAttr("payment_method");
			removeSessionAttr("payment_methods");
			removeSessionAttr("comment");
			removeSessionAttr("order_id");
			removeSessionAttr("coupon");
			removeSessionAttr("reward");
			removeSessionAttr("voucher");
			removeSessionAttr("vouchers");
			redirect(url.linkStr("account/logout", null));
			return;
		}
		language.load("/catalog/language/{0}/account/logout.php");
		document.setTitle(language.getStr("heading_title"));
		List<Map<String, Object>> breadcrumbs = new ArrayList<Map<String,Object>>();
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_home")).put("href", url.linkStr("common/home", null)).toMap());
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_account")).put("href", url.linkStr("account/account", null)).toMap());
		breadcrumbs.add(ChainMap.createMap().put("text", language.getStr("text_logout")).put("href", url.linkStr("account/logout", null)).toMap());
		dataMap.put("breadcrumbs", MemoryUtils.valueOf(breadcrumbs));
		dataMap.put("heading_title",language.get("heading_title"));
		dataMap.put("text_message",language.get("text_message"));
		dataMap.put("button_continue",language.get("button_continue"));
		dataMap.put("continue",url.link("common/home", null));
		dataMap.put("column_left", MemoryUtils.valueOf(load.module(new CommonColumnLeftModule(this))));
		dataMap.put("column_right", MemoryUtils.valueOf(load.module(new CommonColumnRightModule(this))));
		dataMap.put("content_top", MemoryUtils.valueOf(load.module(new CommonContentTopModule(this))));
		dataMap.put("content_bottom", MemoryUtils.valueOf(load.module(new CommonContentBottomModule(this))));
		dataMap.put("footer", MemoryUtils.valueOf(load.module(new CommonFooterModule(this))));
		dataMap.put("header", MemoryUtils.valueOf(load.module(new CommonHeaderModule(this))));
		render(new PhpRender("/catalog/view/theme/default/template/common/success.tpl", dataMap));
	}
}
