package cn.javacart.jopencart.controller.catalog.common;

import com.jfplugin.kit.mockrender.MockRenderKit;

import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;

/**
 * 语言组件
 * @author farmer
 *
 */
public class CommonLanguageModule extends JOpencartModule{

	public CommonLanguageModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/language.tpl", dataMap));
	}

}
