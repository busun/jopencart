package cn.javacart.jopencart;

import javax.servlet.http.HttpServletRequest;

import cn.javacart.jfinal.php.render.PhpEnvPlugin;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jfinal.php.render.PhpRenderFactory;
import cn.javacart.jopencart.controller.catalog.account.AccountAccountController;
import cn.javacart.jopencart.controller.catalog.account.AccountLoginController;
import cn.javacart.jopencart.controller.catalog.account.AccountLogoutController;
import cn.javacart.jopencart.controller.catalog.common.CommonCurrencyModule;
import cn.javacart.jopencart.controller.catalog.common.CommonHomeController;
import cn.javacart.jopencart.controller.catalog.product.ProductCategoryController;
import cn.javacart.jopencart.controller.catalog.product.ProductProductController;
import cn.javacart.jopencart.handler.ImageScalrHandler;
import cn.javacart.jopencart.interceptor.JOpencartInitConfigInterceptor;
import cn.javacart.jopencart.interceptor.JOpencartThreadLocalResetInterceptor;
import cn.javacart.jopencart.kit.SqlObjectKit;
import cn.javacart.jopencart.library.LanguageService;
import cn.javacart.jopencart.model.*;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.alibaba.fastjson.JSON;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.SqlReporter;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.druid.IDruidStatViewAuth;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.template.Engine;
import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 
 * @author farmer
 *
 */
public class JOpencartConfig extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("main_config.properties");//template
		PropKit.use("main_config.properties");
		me.setRenderFactory(new PhpRenderFactory());
		me.setDevMode(getPropertyToBoolean("devMode"));
	}


	@Override
	public void configEngine(Engine me) {
		
	}
	
	@Override
	public void configRoute(Routes me) {
		me.add("/common/home",CommonHomeController.class);
		me.add("/common/currency",CommonCurrencyModule.class);
		me.add("/account/login", AccountLoginController.class);
		me.add("/account/logout",AccountLogoutController.class);
		me.add("/account/account", AccountAccountController.class);
		me.add("/product/product", ProductProductController.class);
		me.add("/product/category", ProductCategoryController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		me.add(new PhpEnvPlugin(getPropertyToBoolean("devMode"), PathKit.getWebRootPath().concat("/").concat(getProperty("template"))));
		DruidPlugin dp = new DruidPlugin(
				PropKit.use("db.properties").get("jdbcUrl"),
				PropKit.use("db.properties").get("user"), 
				PropKit.use("db.properties").get("password")
				);
		dp.set(10, 10, 50);
		dp.addFilter(new StatFilter());
		dp.addFilter(new WallFilter());
		ActiveRecordPlugin ap = new ActiveRecordPlugin(dp);
		ap.setDevMode(getPropertyToBoolean("devMode",false));
		ap.setBaseSqlTemplatePath(PathKit.getWebRootPath()+"/WEB-INF/sql-info/");
		ap.addSqlTemplate("/catalog/product.sql");	//添加SQL
		ap.getSqlKit().getEngine().addSharedStaticMethod(SqlObjectKit.class);
		//设置表前缀
		String dbPrefix = PropKit.use("db.properties").get("db_prefix","joc_");
		ap.getSqlKit().getEngine().addSharedObject("DB_PREFIX", dbPrefix);
		//DB_PREFIX
		me.add(dp);
		me.add(ap);
		ap.setShowSql(getPropertyToBoolean("devMode",false));// 输出SQL
		SqlReporter.setLog(true);
		
		/*========系统默认数据库配置开始=======*/
		ap.addMapping(dbPrefix+"address", "address_id",Address.class);
		ap.addMapping(dbPrefix+"affiliate", "affiliate_id",Affiliate.class);
		ap.addMapping(dbPrefix+"affiliate_activity", "affiliate_activity_id",AffiliateActivity.class);
		ap.addMapping(dbPrefix+"affiliate_login", "affiliate_login_id",AffiliateLogin.class);
		ap.addMapping(dbPrefix+"affiliate_transaction", "affiliate_transaction_id",AffiliateTransaction.class);
		ap.addMapping(dbPrefix+"api", "api_id",Api.class);
		ap.addMapping(dbPrefix+"api_ip", "api_ip_id",ApiIp.class);
		ap.addMapping(dbPrefix+"api_session", "api_session_id",ApiSession.class);
		ap.addMapping(dbPrefix+"attribute", "attribute_id",Attribute.class);
		ap.addMapping(dbPrefix+"attribute_description", "attribute_id,language_id",AttributeDescription.class);
		ap.addMapping(dbPrefix+"attribute_group", "attribute_group_id",AttributeGroup.class);
		ap.addMapping(dbPrefix+"attribute_group_description", "attribute_group_id,language_id",AttributeGroupDescription.class);
		ap.addMapping(dbPrefix+"banner", "banner_id",Banner.class);
		ap.addMapping(dbPrefix+"banner_image", "banner_image_id",BannerImage.class);
		ap.addMapping(dbPrefix+"banner_image_description", "banner_image_id,language_id",BannerImageDescription.class);
		ap.addMapping(dbPrefix+"cart", "cart_id",Cart.class);
		ap.addMapping(dbPrefix+"category", "category_id",Category.class);
		ap.addMapping(dbPrefix+"category_description", "category_id,language_id",CategoryDescription.class);
		ap.addMapping(dbPrefix+"category_filter", "category_id,filter_id",CategoryFilter.class);
		ap.addMapping(dbPrefix+"category_path", "category_id,path_id",CategoryPath.class);
		ap.addMapping(dbPrefix+"category_to_layout", "category_id,store_id",CategoryToLayout.class);
		ap.addMapping(dbPrefix+"category_to_store", "category_id,store_id",CategoryToStore.class);
		ap.addMapping(dbPrefix+"country", "country_id",Country.class);
		ap.addMapping(dbPrefix+"coupon", "coupon_id",Coupon.class);
		ap.addMapping(dbPrefix+"coupon_category", "coupon_id,category_id",CouponCategory.class);
		ap.addMapping(dbPrefix+"coupon_history", "coupon_history_id",CouponHistory.class);
		ap.addMapping(dbPrefix+"coupon_product", "coupon_product_id",CouponProduct.class);
		ap.addMapping(dbPrefix+"currency", "currency_id",Currency.class);
		ap.addMapping(dbPrefix+"custom_field", "custom_field_id",CustomField.class);
		ap.addMapping(dbPrefix+"custom_field_customer_group", "custom_field_id,customer_group_id",CustomFieldCustomerGroup.class);
		ap.addMapping(dbPrefix+"custom_field_description", "custom_field_id,language_id",CustomFieldDescription.class);
		ap.addMapping(dbPrefix+"custom_field_value", "custom_field_value_id",CustomFieldValue.class);
		ap.addMapping(dbPrefix+"custom_field_value_description", "custom_field_value_id,language_id",CustomFieldValueDescription.class);
		ap.addMapping(dbPrefix+"customer", "customer_id",Customer.class);
		ap.addMapping(dbPrefix+"customer_activity", "customer_activity_id",CustomerActivity.class);
		ap.addMapping(dbPrefix+"customer_group", "customer_group_id",CustomerGroup.class);
		ap.addMapping(dbPrefix+"customer_group_description", "customer_group_id,language_id",CustomerGroupDescription.class);
		ap.addMapping(dbPrefix+"customer_history", "customer_history_id",CustomerHistory.class);
		ap.addMapping(dbPrefix+"customer_ip", "customer_ip_id",CustomerIp.class);
		ap.addMapping(dbPrefix+"customer_login", "customer_login_id",CustomerLogin.class);
		ap.addMapping(dbPrefix+"customer_online", "ip",CustomerOnline.class);
		ap.addMapping(dbPrefix+"customer_reward", "customer_reward_id",CustomerReward.class);
		ap.addMapping(dbPrefix+"customer_transaction", "customer_transaction_id",CustomerTransaction.class);
		ap.addMapping(dbPrefix+"customer_wishlist", "customer_id,product_id",CustomerWishlist.class);
		ap.addMapping(dbPrefix+"download", "download_id",Download.class);
		ap.addMapping(dbPrefix+"download_description", "download_id,language_id",DownloadDescription.class);
		ap.addMapping(dbPrefix+"event", "event_id",Event.class);
		ap.addMapping(dbPrefix+"extension", "extension_id",Extension.class);
		ap.addMapping(dbPrefix+"filter", "filter_id",Filter.class);
		ap.addMapping(dbPrefix+"filter_description", "filter_id,language_id",FilterDescription.class);
		ap.addMapping(dbPrefix+"filter_group", "filter_group_id",FilterGroup.class);
		ap.addMapping(dbPrefix+"filter_group_description", "filter_group_id,language_id",FilterGroupDescription.class);
		ap.addMapping(dbPrefix+"geo_zone", "geo_zone_id",GeoZone.class);
		ap.addMapping(dbPrefix+"information", "information_id",Information.class);
		ap.addMapping(dbPrefix+"information_description", "information_id,language_id",InformationDescription.class);
		ap.addMapping(dbPrefix+"information_to_layout", "information_id,store_id",InformationToLayout.class);
		ap.addMapping(dbPrefix+"information_to_store", "information_id,store_id",InformationToStore.class);
		ap.addMapping(dbPrefix+"language", "language_id",Language.class);
		ap.addMapping(dbPrefix+"layout", "layout_id",Layout.class);
		ap.addMapping(dbPrefix+"layout_module", "layout_module_id",LayoutModule.class);
		ap.addMapping(dbPrefix+"layout_route", "layout_route_id",LayoutRoute.class);
		ap.addMapping(dbPrefix+"length_class", "length_class_id",LengthClass.class);
		ap.addMapping(dbPrefix+"length_class_description", "length_class_id,language_id",LengthClassDescription.class);
		ap.addMapping(dbPrefix+"location", "location_id",Location.class);
		ap.addMapping(dbPrefix+"manufacturer", "manufacturer_id",Manufacturer.class);
		ap.addMapping(dbPrefix+"manufacturer_to_store", "manufacturer_id,store_id",ManufacturerToStore.class);
		ap.addMapping(dbPrefix+"marketing", "marketing_id",Marketing.class);
		ap.addMapping(dbPrefix+"modification", "modification_id",Modification.class);
		ap.addMapping(dbPrefix+"module", "module_id",Module.class);
		ap.addMapping(dbPrefix+"option", "option_id",Option.class);
		ap.addMapping(dbPrefix+"option_description", "option_id,language_id",OptionDescription.class);
		ap.addMapping(dbPrefix+"option_value", "option_value_id",OptionValue.class);
		ap.addMapping(dbPrefix+"option_value_description", "option_value_id,language_id",OptionValueDescription.class);
		ap.addMapping(dbPrefix+"order", "order_id",Order.class);
		ap.addMapping(dbPrefix+"order_custom_field", "order_custom_field_id",OrderCustomField.class);
		ap.addMapping(dbPrefix+"order_history", "order_history_id",OrderHistory.class);
		ap.addMapping(dbPrefix+"order_option", "order_option_id",OrderOption.class);
		ap.addMapping(dbPrefix+"order_product", "order_product_id",OrderProduct.class);
		ap.addMapping(dbPrefix+"order_recurring", "order_recurring_id",OrderRecurring.class);
		ap.addMapping(dbPrefix+"order_recurring_transaction", "order_recurring_transaction_id",OrderRecurringTransaction.class);
		ap.addMapping(dbPrefix+"order_status", "order_status_id,language_id",OrderStatus.class);
		ap.addMapping(dbPrefix+"order_total", "order_total_id",OrderTotal.class);
		ap.addMapping(dbPrefix+"order_voucher", "order_voucher_id",OrderVoucher.class);
		ap.addMapping(dbPrefix+"product", "product_id",Product.class);
		ap.addMapping(dbPrefix+"product_attribute", "product_id,attribute_id,language_id",ProductAttribute.class);
		ap.addMapping(dbPrefix+"product_description", "product_id,language_id",ProductDescription.class);
		ap.addMapping(dbPrefix+"product_discount", "product_discount_id",ProductDiscount.class);
		ap.addMapping(dbPrefix+"product_filter", "product_id,filter_id",ProductFilter.class);
		ap.addMapping(dbPrefix+"product_image", "product_image_id",ProductImage.class);
		ap.addMapping(dbPrefix+"product_option", "product_option_id",ProductOption.class);
		ap.addMapping(dbPrefix+"product_option_value", "product_option_value_id",ProductOptionValue.class);
		ap.addMapping(dbPrefix+"product_recurring", "product_id,recurring_id,customer_group_id",ProductRecurring.class);
		ap.addMapping(dbPrefix+"product_related", "product_id,related_id",ProductRelated.class);
		ap.addMapping(dbPrefix+"product_reward", "product_reward_id",ProductReward.class);
		ap.addMapping(dbPrefix+"product_special", "product_special_id",ProductSpecial.class);
		ap.addMapping(dbPrefix+"product_to_category", "product_id,category_id",ProductToCategory.class);
		ap.addMapping(dbPrefix+"product_to_download", "product_id,download_id",ProductToDownload.class);
		ap.addMapping(dbPrefix+"product_to_layout", "product_id,store_id",ProductToLayout.class);
		ap.addMapping(dbPrefix+"product_to_store", "product_id,store_id",ProductToStore.class);
		ap.addMapping(dbPrefix+"recurring", "recurring_id",Recurring.class);
		ap.addMapping(dbPrefix+"recurring_description", "recurring_id,language_id",RecurringDescription.class);
		ap.addMapping(dbPrefix+"return", "return_id",Return.class);
		ap.addMapping(dbPrefix+"return_action", "return_action_id,language_id",ReturnAction.class);
		ap.addMapping(dbPrefix+"return_history", "return_history_id",ReturnHistory.class);
		ap.addMapping(dbPrefix+"return_reason", "return_reason_id,language_id",ReturnReason.class);
		ap.addMapping(dbPrefix+"return_status", "return_status_id,language_id",ReturnStatus.class);
		ap.addMapping(dbPrefix+"review", "review_id",Review.class);
		ap.addMapping(dbPrefix+"setting", "setting_id",Setting.class);
		ap.addMapping(dbPrefix+"stock_status", "stock_status_id,language_id",StockStatus.class);
		ap.addMapping(dbPrefix+"store", "store_id",Store.class);
		ap.addMapping(dbPrefix+"tax_class", "tax_class_id",TaxClass.class);
		ap.addMapping(dbPrefix+"tax_rate", "tax_rate_id",TaxRate.class);
		ap.addMapping(dbPrefix+"tax_rate_to_customer_group", "tax_rate_id,customer_group_id",TaxRateToCustomerGroup.class);
		ap.addMapping(dbPrefix+"tax_rule", "tax_rule_id",TaxRule.class);
		ap.addMapping(dbPrefix+"upload", "upload_id",Upload.class);
		ap.addMapping(dbPrefix+"url_alias", "url_alias_id",UrlAlias.class);
		ap.addMapping(dbPrefix+"user", "user_id",User.class);
		ap.addMapping(dbPrefix+"user_group", "user_group_id",UserGroup.class);
		ap.addMapping(dbPrefix+"voucher", "voucher_id",Voucher.class);
		ap.addMapping(dbPrefix+"voucher_history", "voucher_history_id",VoucherHistory.class);
		ap.addMapping(dbPrefix+"voucher_theme", "voucher_theme_id",VoucherTheme.class);
		ap.addMapping(dbPrefix+"voucher_theme_description", "voucher_theme_id,language_id",VoucherThemeDescription.class);
		ap.addMapping(dbPrefix+"weight_class", "weight_class_id",WeightClass.class);
		ap.addMapping(dbPrefix+"weight_class_description", "weight_class_id,language_id",WeightClassDescription.class);
		ap.addMapping(dbPrefix+"zone", "zone_id",Zone.class);
		ap.addMapping(dbPrefix+"zone_to_geo_zone", "zone_to_geo_zone_id",ZoneToGeoZone.class);
		/*========系统默认数据库配置结束=======*/
		me.add(new EhCachePlugin());
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new JOpencartInitConfigInterceptor());	//初始化Opencart相关
		me.add(new JOpencartThreadLocalResetInterceptor());	//重置线程变量
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new ImageScalrHandler());
		me.add(new ContextPathHandler());
		me.add(new DruidStatViewHandler("/druid", new IDruidStatViewAuth() {
			@Override
			public boolean isPermitted(HttpServletRequest request) {
				return true;
			}
		}));
	}

	@Override
	public void afterJFinalStart() {
		LogKit.info(MockRenderKit.render(new PhpRender("/start.php")));
		JSON.parse("{}");
		LanguageService.getInstance().loadDefault("/catalog/language/en-gb/en-gb.php");	//初始化加载语言
		super.afterJFinalStart();
	}
	
	@Override
	public void beforeJFinalStop() {
		super.beforeJFinalStop();
	}
}
