package cn.javacart.jopencart.engine;

import cn.javacart.jopencart.library.ConfigService;
import cn.javacart.jopencart.library.cart.TaxService;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * 
 * @author farmer
 *
 */
public abstract class JOpencartInterceptor implements Interceptor{

	protected ConfigService config = null;
	
	protected TaxService tax = null;
	
	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		if((controller instanceof JOpencartController)){
			config = ((JOpencartController) controller).config;
			tax =  ((JOpencartController) controller).tax;
		}
		doIntercept(inv, (controller instanceof JOpencartController) ? (JOpencartController)controller : null);
	}
	
	public abstract void doIntercept(Invocation inv ,JOpencartController controller);
	
}
