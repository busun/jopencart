package cn.javacart.jopencart.engine;

import java.util.LinkedHashMap;
import java.util.Map;

import cn.javacart.jopencart.library.ConfigService;
import cn.javacart.jopencart.library.LanguageService;
import cn.javacart.jopencart.library.cart.CustomerService;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * 
 * @author farmer
 *
 */
public abstract class JOpencartValidator extends Validator{

	/**
	 * 语言
	 */
	protected LanguageService language = null;
	
	/**
	 * 配置
	 */
	protected ConfigService config = null;
	
	/**
	 * 客户当前客户对象
	 */
	protected CustomerService customer = null;
	
	/**
	 * 存储错误信息
	 */
	protected Map<String, String> errMap = new LinkedHashMap<String, String>();
	
	protected void addError(String errorKey, String errorMessage) {
		super.addError(errorKey, errorMessage);
		errMap.put(errorKey, errorMessage);
	}

	@Override
	protected void validate(Controller c) {
		if(c instanceof JOpencartController){
			this.language = ((JOpencartController) c).language;
			this.config = ((JOpencartController) c).config;
			this.customer = ((JOpencartController) c).customer;
			doValidate((JOpencartController)c);
		}
	}
	
	protected abstract void doValidate(JOpencartController c);
	
	@Override
	protected void handleError(Controller c) {
		if(c instanceof JOpencartController){
			((JOpencartController) c).setErrorMap(errMap);
		}
		invocation.invoke();
	}
	
}
