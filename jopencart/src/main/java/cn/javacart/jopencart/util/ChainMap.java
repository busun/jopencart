package cn.javacart.jopencart.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 链式语法Map
 * @author farmer
 *
 */
public class ChainMap implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> map = new HashMap<String, Object>();
	
	public  static ChainMap createMap(){
		return new ChainMap();
	}
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public ChainMap put(String key ,Object value){
		map.put(key, value);
		return this;
	}
	
	/**
	 * 
	 * @return
	 */
	public Map<String, Object> toMap(){
		return map;
	}
}
