/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_affiliate_login表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|affiliate_login_id  |INT(10)             |false |true    |NULL    |
|email               |VARCHAR(96)         |false |false   |NULL    |
|ip                  |VARCHAR(40)         |false |false   |NULL    |
|total               |INT(10)             |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class AffiliateLogin extends Model<AffiliateLogin>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2336411483282233641L;
	/**
	 * 用于查询操作
	 */
	public static final AffiliateLogin ME = new AffiliateLogin();
	
}