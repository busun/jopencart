/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_weight_class表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|weight_class_id     |INT(10)             |false |true    |NULL    |
|value               |DECIMAL(15)         |false |false   |0.00000000|
+--------------------+--------------------+------+--------+--------+--------

 */
public class WeightClass extends Model<WeightClass>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2494481483282249448L;
	/**
	 * 用于查询操作
	 */
	public static final WeightClass ME = new WeightClass();
	
}