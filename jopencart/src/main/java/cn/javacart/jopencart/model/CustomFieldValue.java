/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_custom_field_value表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|custom_field_value_id|INT(10)             |false |true    |NULL    |
|custom_field_id     |INT(10)             |false |false   |NULL    |
|sort_order          |INT(10)             |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class CustomFieldValue extends Model<CustomFieldValue>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2375851483282237585L;
	/**
	 * 用于查询操作
	 */
	public static final CustomFieldValue ME = new CustomFieldValue();
	
}