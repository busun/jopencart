/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_marketing表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|marketing_id        |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(32)         |false |false   |NULL    |
|description         |TEXT(65535)         |false |false   |NULL    |
|code                |VARCHAR(64)         |false |false   |NULL    |
|clicks              |INT(10)             |false |false   |0|
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Marketing extends Model<Marketing>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2420871483282242087L;
	/**
	 * 用于查询操作
	 */
	public static final Marketing ME = new Marketing();
	
}