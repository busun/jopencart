/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
jjoc_category表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|category_id         |INT(10)             |false |true    |NULL    |
|image               |VARCHAR(255)        |true  |false   |NULL    |
|parent_id           |INT(10)             |false |false   |0|
|top                 |BIT(0)              |false |false   |NULL    |
|column              |INT(10)             |false |false   |NULL    |
|sort_order          |INT(10)             |false |false   |0|
|status              |BIT(0)              |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Category extends Model<Category>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2354611483282235461L;
	/**
	 * 用于查询操作
	 */
	public static final Category ME = new Category();
	
	/**
	 * 
	 * @param parent_id
	 * @return
	 */
	public List<Category> getCategories(int parent_id){
		
		return Category.ME.findByCache("APP_CACHE", "getCategories_"+parent_id, "SELECT * FROM joc_category c LEFT JOIN joc_category_description cd ON (c.category_id = cd.category_id) LEFT JOIN joc_category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = ?"
				+ "	 AND cd.language_id = ? AND c2s.store_id = ?"
				+ "  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)",parent_id,1,0);
		
		/*
		return Category.ME.find("SELECT * FROM joc_category c LEFT JOIN joc_category_description cd ON (c.category_id = cd.category_id) LEFT JOIN joc_category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = ?"
				+ "	 AND cd.language_id = ? AND c2s.store_id = ?"
				+ "  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)",parent_id,1,0);
				*/
	}
	
}