/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_user表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|user_id             |INT(10)             |false |true    |NULL    |
|user_group_id       |INT(10)             |false |false   |NULL    |
|username            |VARCHAR(20)         |false |false   |NULL    |
|password            |VARCHAR(40)         |false |false   |NULL    |
|salt                |VARCHAR(9)          |false |false   |NULL    |
|firstname           |VARCHAR(32)         |false |false   |NULL    |
|lastname            |VARCHAR(32)         |false |false   |NULL    |
|email               |VARCHAR(96)         |false |false   |NULL    |
|image               |VARCHAR(255)        |false |false   |NULL    |
|code                |VARCHAR(40)         |false |false   |NULL    |
|ip                  |VARCHAR(40)         |false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class User extends Model<User>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2486771483282248677L;
	/**
	 * 用于查询操作
	 */
	public static final User ME = new User();
	
}