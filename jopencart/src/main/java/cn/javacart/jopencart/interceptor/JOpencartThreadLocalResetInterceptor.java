package cn.javacart.jopencart.interceptor;

import cn.javacart.jopencart.library.DocumentService;
import cn.javacart.jopencart.library.LanguageService;
import cn.javacart.jopencart.library.RequestService;
import cn.javacart.jopencart.library.SessionConfigService;
import cn.javacart.jopencart.library.SessionService;
import cn.javacart.jopencart.library.cart.TaxService;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * 一些线程变量重置
 * @author farmer
 *
 */
public class JOpencartThreadLocalResetInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		inv.invoke();
		DocumentService.reset();//处理完毕，重置Document
		LanguageService.reset();
		SessionConfigService.reset();
		RequestService.reset();
		SessionService.reset();
		TaxService.reset();
	}

}
